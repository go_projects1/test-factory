package bootstrap

import (
	"context"
	"fmt"
	"hamkor-mobile/test-factory/adapters/services"
	"hamkor-mobile/test-factory/adapters/services/service_registry"
	"hamkor-mobile/test-factory/config"
	"hamkor-mobile/test-factory/controller"
	"hamkor-mobile/test-factory/pkg/logger"
)

type App struct {
	//	main config
	teardown   []func()
	controller *controller.Controller
	container  services.Container
}

func New(cfg config.Config) *App {

	teardown := make([]func(), 0)
	log := logger.New(cfg.LogLevel, "dbo_physical_onboarding")
	serviceContainer := service_registry.New(cfg, log)
	controller := controller.New(log,&serviceContainer)
	app := App{
		teardown:   teardown,
		controller: &controller,
		container:  &serviceContainer,
	}

	return &app
}

func (app *App) Run(ctx context.Context) {
/*	go func(c context.Context) {
		app.mqPool.Run(c)
	}(ctx)*/

	refServiceTest(app,ctx)


	<-ctx.Done()
	for i := range app.teardown {
		app.teardown[i]()
	}


}

func refServiceTest(app *App,ctx context.Context){
	//Find PS by code
	ps,err := app.controller.GetProcessingByCode(ctx,"UZCARD")
	fmt.Println(ps)
	if err != nil {
		fmt.Println(err)
	}
	// Find processing systems
	pss,err := app.controller.GetProcessings(ctx)
	fmt.Println(pss)
	if err != nil {
		fmt.Println(err)
	}

	//Find bin by number
	bin,err := app.controller.GetCardBinByNumber(ctx,"8600120480409831")
	fmt.Println(bin)
	if err != nil {
		fmt.Println(err)
	}
	// Find bins
	bins,err := app.controller.GetCardBins(ctx)
	fmt.Println(bins)
	if err != nil {
		fmt.Println(err)
	}
}
