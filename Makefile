run:
	go run cmd/main.go
	
mkdrs:
	mkdir "drivers"
	mkdir "bootstrap"
	mkdir "cmd"
	mkdir "docs"
	mkdir "errors"
	mkdir "config"
	mkdir "controller"
	mkdir "domains"
	mkdir "usecases"
	mkdir "ports"
	mkdir "tools"
	mkdir "migrations"
	mkdir "drivers/services"
	mkdir "drivers/store"
	mkdir "ports/mq"
	mkdir "ports/rest"
	mkdir "ports/grpc"
	mkdir "ports/schedulers"