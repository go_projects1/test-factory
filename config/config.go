package config

import (
	"github.com/joho/godotenv"
	"github.com/spf13/cast"
	"log"
	"os"
)

type Config struct {
	LogLevel 	   string
	RefServiceGRPC string
}

// Load ...
func Load() Config {
	if err := godotenv.Load(); //load .env file
		err != nil {
		log.Print("No .env file found")
	}
	config := Config{}
	config.LogLevel = cast.ToString(getOrReturnDefaultValue("LOG_LEVEL", "debug"))
	config.RefServiceGRPC = cast.ToString(getOrReturnDefaultValue("REF_SERVICE_GRPC", "localhost:7577"))
	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)

	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}