package controller

import (
	"context"
	"hamkor-mobile/test-factory/domains"
)



func (ctrl *Controller) GetProcessingByCode(ctx context.Context, code string) (res domains.ProcessingSystem,err error) {
	res, err = ctrl.services.RefServiceGrpc().GetProcessingSystem(code)
	if err != nil {
		return
	}
	return
}

func (ctrl *Controller) GetProcessings(ctx context.Context) (res []domains.ProcessingSystem,err error) {
	res, err = ctrl.services.RefServiceGrpc().GetProcessingSystems()
	if err != nil {
		return
	}
	return
}

func (ctrl *Controller) GetCardBinByNumber(ctx context.Context, number string) (res domains.CardBin,err error) {
	res, err = ctrl.services.RefServiceGrpc().GetCardBin(number)
	if err != nil {
		return
	}
	return
}

func (ctrl *Controller) GetCardBins(ctx context.Context) (res []domains.CardBin,err error) {
	res, err = ctrl.services.RefServiceGrpc().GetCardBins()
	if err != nil {
		return
	}
	return
}
