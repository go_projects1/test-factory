package controller

import (
	"fmt"
	"hamkor-mobile/test-factory/adapters/services"
	"hamkor-mobile/test-factory/pkg/logger"
)

type Controller struct {
	log      logger.Logger
	services services.Container
}

func New(l logger.Logger, services services.Container) Controller {
	return Controller{
		log:      l,
		services: services,
	}
}

func (ctrl Controller) errorf(tmpl string, params ...interface{}) {
	ctrl.log.Error(fmt.Sprintf("controller: "+tmpl, params...))
}

func (ctrl Controller) debugf(tmpl string, params ...interface{}) {
	ctrl.log.Debug(fmt.Sprintf("controller: "+tmpl, params...))
}

func (ctrl Controller) infof(tmpl string, params ...interface{}) {
	ctrl.log.Debug(fmt.Sprintf("controller: "+tmpl, params...))
}
