package services

import "hamkor-mobile/test-factory/domains"

type Container interface {
	RefServiceGrpc() RefServiceGrpc
}

type RefServiceGrpc interface {
	GetProcessingSystem(code string) (domains.ProcessingSystem, error)
	GetProcessingSystems() ([]domains.ProcessingSystem, error)
	GetCardBin(number string)(domains.CardBin,error)
	GetCardBins()([]domains.CardBin,error)
}
