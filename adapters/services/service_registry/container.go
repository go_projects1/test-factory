package service_registry

import (
	"hamkor-mobile/test-factory/adapters/services"
	"hamkor-mobile/test-factory/config"
	"hamkor-mobile/test-factory/pkg/logger"
	"time"
)

const requestTimeout = time.Second * 10

type Container struct {
  refService *RefService
}

func (c *Container) RefServiceGrpc() services.RefServiceGrpc {
	return c.refService
}

func New(c config.Config, l logger.Logger) Container {
  return Container{refService: newRefService(c.RefServiceGRPC,l)}
}


