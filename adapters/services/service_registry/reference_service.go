package service_registry

import (
	"context"
	proto "gitlab.hamkorbank.uz/libs/protos/genproto/ref_service"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
	"hamkor-mobile/test-factory/domains"
	"hamkor-mobile/test-factory/pkg/logger"
)

type RefService struct {
	grpcClient proto.ReferenceServiceClient
	logger.Logger
}

func (s *RefService) GetProcessingSystem(code string) (domains.ProcessingSystem, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	resp, err := s.grpcClient.ReadProcessing(ctx, &proto.ReadProcessingReq{Code: code})
	ps := domains.ProcessingSystem{}
	if err != nil {
		return ps, err
	}
	ps.ID = resp.ProcessingSystem.ID
	ps.Code = resp.ProcessingSystem.Code
	ps.Name = resp.ProcessingSystem.Name
	ps.IsPayAllowed = resp.ProcessingSystem.IsPayAllowed
	return ps, nil
}

func (s *RefService) GetProcessingSystems() ([]domains.ProcessingSystem, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	resp, err := s.grpcClient.ReadAllProcessings(ctx,&emptypb.Empty{})
	ps := []domains.ProcessingSystem{}
	if err != nil {
		return ps, err
	}
	for _,p := range resp.ProcessingSystem {
		var v = domains.ProcessingSystem{}
		v.ID = p.ID
		v.Code = p.Code
		v.Name = p.Name
		v.IsPayAllowed = p.IsPayAllowed
		ps = append(ps,v)
	}
	return ps, nil
}

func (s *RefService) GetCardBin(number string) (domains.CardBin, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	resp, err := s.grpcClient.ReadCardBin(ctx, &proto.ReadCardBinReq{Number: number})
	ps := domains.CardBin{}
	if err != nil {
		return ps, err
	}
	ps.ID = resp.CardBin.ID
	ps.Number = resp.CardBin.Number
	ps.Name = resp.CardBin.Name
	ps.IsBankCard = resp.CardBin.IsBankCard
	ps.IsAddAllowed = resp.CardBin.IsAddAllowed
	ps.CurrencyCode = resp.CardBin.CurrencyCode
	ps.Processing.Code = resp.CardBin.ProcessingCode
	ps.Processing.IsPayAllowed = resp.CardBin.IsPayAllowed
	return ps, nil
}

func (s *RefService) GetCardBins() ([]domains.CardBin, error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	resp, err := s.grpcClient.ReadAllCardBins(ctx, &emptypb.Empty{})
	pss := []domains.CardBin{}
	if err != nil {
		return pss, err
	}
	for _,c := range resp.CardBin {
		ps := domains.CardBin{}
		ps.ID = c.ID
		ps.Number = c.Number
		ps.Name = c.Name
		ps.IsBankCard = c.IsBankCard
		ps.IsAddAllowed = c.IsAddAllowed
		ps.CurrencyCode = c.CurrencyCode
		ps.Processing.Code = c.ProcessingCode
		ps.Processing.IsPayAllowed = c.IsPayAllowed
		pss = append(pss,ps)
	}
	return pss, nil
}

// init ref serc
func newRefService(host string, log logger.Logger) *RefService {
	c, err := grpc.Dial(host, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	return &RefService{grpcClient: proto.NewReferenceServiceClient(c), Logger: log}
}