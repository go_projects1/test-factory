package main

import (
	"context"
	"fmt"
	"hamkor-mobile/test-factory/bootstrap"
	"hamkor-mobile/test-factory/config"
	"hamkor-mobile/test-factory/pkg/logger"
	"os"
	"os/signal"
)

func main()  {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	cfg := config.Load()
	l := logger.New(cfg.LogLevel, "test-factory")
	application := bootstrap.New(cfg)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		l.Info(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()
	application.Run(ctx)

}
