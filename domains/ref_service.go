package domains

type ProcessingSystem struct {
	ID           string
	Code         string
	Name         string
	IsPayAllowed bool
}

type CardBin struct {
	ID 			 string
	Number       string
	Name 		 string
	CurrencyCode string
	IsBankCard   bool
	IsAddAllowed bool
	Processing   ProcessingSystem
}
