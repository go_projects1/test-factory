module hamkor-mobile/test-factory

go 1.15

require (
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.4.1
	gitlab.hamkorbank.uz/libs/protos v0.1.23
	go.uber.org/zap v1.19.1
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
